// local dev mode
export const environment = {
  production: false,
  loglevel: 'DEBUG',
  maxLogs: undefined,
  backend: 'http://localhost:8000',
  apiPath: 'http://localhost:8000/api',
  ssl: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
