// staging mode
export const environment = {
  production: true,
  loglevel: 'INFO',
  maxLogs: 4000,
  backend: '',
  apiPath: '/api',
  ssl: false
};
