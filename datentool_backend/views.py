from .area.views import *
from .user.views import *
from .population.views import *
from .modes.views import *
from .demand.views import *
from .infrastructure.views import *
from .site.views import *
from .indicators.views import *
from .places.views import *
from .logging.views import *

from rest_framework.views import exception_handler
from django.http import JsonResponse

def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)
    if response and not response.content_type == 'application/json' \
       and hasattr(exc, 'detail'):
        err_message = ''
        if type(exc.detail) == list:
            for err in exc.detail:
                err_message += (err.encode()).decode() + '\n'
        else:
            err_message = exc.detail
        json_res = JsonResponse(
            {
                "error": {
                    "status_code": response.status_code,
                    "message": err_message,
                    "detail": err_message
                }
            }, status=response.status_code
        )
        return json_res
    return response